## Laravel test project
Laravel-project:
- [Link to the project - T9](https://drive.google.com/file/d/1WGbkHLG00pj5UH0E151SllVIzI35tzo6/view)

Installation of the project:
- настроить файл .env под себя, в частности:
 
    - MAIL_MAILER=smtp
    - MAIL_HOST=smtp.googlemail.com
    - MAIL_PORT=465
    - MAIL_USERNAME=[email]
    - MAIL_PASSWORD=[password]
    - MAIL_ENCRYPTION=ssl
    - MAIL_FROM_ADDRESS=[working_address]
    - MAIL_FROM_NAME="${APP_NAME}"
      
- php artisan migrate
- composer require spatie/laravel-permission
- db:seed; -- 
    не успел починить фабрики, но запускать всё равно нужно
    чтобы создать роли / учётку менеджера: настроил чтобы
    сиды кроме этих целей ничего не создавали
    
Учётка менеджера
manager@gmail.com
1as2d3asd4asd5sad6sads7a8d9dsdf