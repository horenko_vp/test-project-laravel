@component('mail::message')
# {{ $msg->title }}.
<p>{{ $msg->description }}</p>
<p><i style="font-size: 13px;">{{ $msg->created_at }}</i></p>

@component('mail::button', ['url' => action('ApplicationController@show', ['application' => $application->id])])
Go to newly-created application
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
