@extends('layouts.app')
@section('head-add')
    <link rel="stylesheet" href="{{ asset('css/show.css') }}">
@endsection

@section('content')
    <div class="jumbotron">
        <h2>{{ $head_message->title }}</h2>
        <p class="date_sm d-inline-block">{{ $head_message->created_at }}</p><p class="float-right"><b>{{ $application->client->email }}</b></p>
        <p>{{ $head_message->description }}</p>
        @if($head_message->file)
            Attached file: <a href="{{ action('ApplicationController@download', ['file' => $head_message->file->id]) }}">{{ $head_message->file->get_public_filename() }}</a>
        @endif
        <hr>
        @foreach($messages as $msg)
            <div class="w-100 mt-3 {{ $loop->index % 2 == 0 ? "msg_light" : "msg_dark" }}">
                <h5 class="d-inline-block">{{ $msg->title }}</h5><p class="float-right"><b>{{ $msg->sender->email }}</b></p>
                <p class="date_sm">{{ $msg->created_at }}</p>

                <p>{{ $msg->description }}</p>
                @if($msg->file)
                    <p>Attached file: <a href="{{ action('ApplicationController@download', ['file' => $msg->file->id]) }}">{{ $msg->file->get_public_filename() }}</a></p>
                @endif
            </div>
        @endforeach

        @if($application->is_closed)
            <hr>
            <h4>Application is finished. No more replies are necessary</h4>
        @elseif(!$application->manager_id and \Illuminate\Support\Facades\Auth::user()->hasRole('Manager'))
            <a href="{{ action('ApplicationController@start', ['application' => $application->id]) }}" class="btn btn-success">Start conversation</a>
        @else
            <form method="post" style="width: 30%" class="mt-4 align-middle" enctype="multipart/form-data">
                @csrf()
                <div class="form-group">
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $errors->get('title')[0] }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <input type="text" class="form-control" name="title" placeholder="Enter title" id="title">
                </div>
                <div class="form-group">
                    @if(count($errors)>0)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $errors->get('description')[0] }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <textarea name="description" class="form-control" placeholder="Description" id="description" cols="15" rows="10" style="height: 6.5rem;"></textarea>
                </div>
                <div class="form-group">
                    <div class="upload-btn-wrapper" style="float: left">
                        <button class="btnc">Upload a file</button>
                        <input type="file" class="form-control-file" name="file" />
                    </div>
                    <div style="float: left; margin-left: 2%;"><button type="submit" id="btn_s__" class="btn btnc ml-3;">Send</button></div>
                </div>
            </form>
            @if(\Illuminate\Support\Facades\Auth::user()->id == $application->client_id)
                <div class="float-right">
                    <a href="{{ action('ApplicationController@finish', ['application' => $application->id]) }}" class="btn btn-danger">Finish the application</a>
                </div>
            @endif
        @endif
    </div>
    <script src="{{ asset('js/show/index.js') }}"></script>
@endsection
