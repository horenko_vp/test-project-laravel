@extends('layouts.app')
@section('head-add')
    <link rel="stylesheet" href="{{ asset('css/show.css') }}">
@endsection

@section('content')
    <div class="jumbotron">
        @if(count($errors)>0)
            {{ $errors }}
        @endif

        <form method="post" style="width: 30%" action="{{ action('ApplicationController@store') }}"
              class="align-middle" enctype="multipart/form-data">
            @csrf()
            <div class="form-group">
                <input type="text" class="form-control" required name="title" placeholder="Enter title" id="title">
            </div>
            <div class="form-group">
                <textarea name="description" required class="form-control" placeholder="Description" id="description" cols="15" rows="10" style="height: 6.5rem;"></textarea>
            </div>
            <div class="form-group">
                <div class="upload-btn-wrapper" style="float: left">
                    <button class="btnc">Upload a file</button>
                    <input type="file" class="form-control-file" name="file" />
                </div>
                <input type="hidden" name="__creating" value="true">
                <div style="float: left; margin-left: 2%;"><button type="submit" id="btn_s__" class="btn btnc">Create</button></div>
            </div>
        </form>
    </div>
    <script src="{{ asset('js/show/index.js') }}"></script>
@endsection
