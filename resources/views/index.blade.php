@extends('layouts.app')
@section('head-add')
    <link rel="stylesheet" href="{{ asset('css/index.css') }}">
@endsection
@section('content')
        @if(count($applications))
            <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th class="th-sm" scope="col">#</th>
                    <th class="th-sm" scope="col">Title</th>
                    <th class="th-sm" scope="col">Description</th>
                    <th class="th-sm" scope="col">Manager</th>
                    <th class="th-sm" scope="col">Created at</th>
                    <th class="th-sm" scope="col">Is closed</th>
                </tr>
                </thead>
                <tbody>
                @foreach($applications as $application)
                    <tr>
                        <th scope="row">
                            <a href="{{ action('ApplicationController@show', ['application' => $application->id]) }}">
                                <div style="width: 100%;height: 100%;color:blue;">
                                    {{ $application->id }}
                                </div>
                            </a>
                        </th>
                        <td>{{ $application->messages->first()->title }}</td>
                        <td>{{ \Illuminate\Support\Str::limit($application->messages->first()->description, 50) }}</td>
                        <td>{{ $application->manager->email ?? 'No manager'}}</td>
                        <td>{{ $application->created_at}}</td>
                        <td>{{ $application->is_closed ? 'Yes' : 'No'}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <!-- JQuery -->
            <script src="https://code.jquery.com/jquery-3.5.0.min.js" integrity="sha256-xNzN2a4ltkB44Mc/Jz3pT4iU1cmeR0FkXs4pru/JxaQ=" crossorigin="anonymous"></script>
            <!-- MDB core JavaScript -->
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.16.0/js/mdb.min.js"></script>
            <!-- MDBootstrap Datatables  -->
            <script type="text/javascript" defer src="{{ asset('packages/mdb/js/addons/datatables.min.js') }}"></script>
            <script src="{{ asset('js/index/index.js') }}"></script>
        @else
            @if(\Illuminate\Support\Facades\Auth::user()->hasRole('Manager'))
                <p>There's no any applications published</p>
            @else
                <p>You haven't published any applications yet</p>
            @endif
        @endif
@endsection
