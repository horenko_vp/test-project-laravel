<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageFile extends Model
{
    public static $path_to_user_files = DIRECTORY_SEPARATOR . 'application'. DIRECTORY_SEPARATOR .'msgs' . DIRECTORY_SEPARATOR .'user_files' . DIRECTORY_SEPARATOR;
    public static $uuid_name_separator = '_^_^_^_^_^_^_';

    protected $fillable = ['filename', 'message_id'];
    public function message() {
        return $this->hasOne(Message::class, 'message_id', 'id');
    }
    public function get_public_filename() {
        $split_array = explode(self::$uuid_name_separator, $this->filename);
        return $split_array[count($split_array)-1];
    }
}
