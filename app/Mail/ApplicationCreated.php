<?php

namespace App\Mail;

use App\Application;
use App\MessageFile;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplicationCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $application;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_response = $this
            ->from(config('mail.from.address'))
            ->markdown('emails.applications.created')
            ->with([
                'application' => $this->application,
                'msg' => $this->application->messages->first()
            ]);
        if($this->application->messages->first()->file) {
            $filename = $this->application->messages->first()->file->filename;
            $mail_response->attach(public_path() . MessageFile::$path_to_user_files . $filename);
        }
        return $mail_response;
    }
}
