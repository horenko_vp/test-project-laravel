<?php

namespace App\Http\Requests;

use App\Application;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Validator;

class StoreMessagePostRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title' => 'required|string',
            'description' => 'required|string',
            'file' => 'nullable|file'
        ];
    }

    public function authorize()
    {
        /*
        $application = Application::all()->find($this->route('application'));
        $user = $this->user();

        $condition =
            ($user->id == $application->client_id or $user->id == $application->manager_id);
        */
        return true /* $condition */;
    }
}
