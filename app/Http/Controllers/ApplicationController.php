<?php

namespace App\Http\Controllers;

use App\Application;
use App\Http\Requests\StoreMessagePostRequest;
use App\Mail\ApplicationClosed;
use App\Mail\ApplicationCreated;
use App\Mail\ApplicationMessageCreated;
use App\Message;
use App\MessageFile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{
    public function download(Request $request, MessageFile $file) {
        return response()->download(public_path() . MessageFile::$path_to_user_files . $file->filename, $file->get_public_filename());
    }
    public function create(Request $request) {
        return response()
                ->view('create');
    }
    public function index(Request $request) {
        $context = null;
        if($request->user()->hasRole('Manager')) {
            $context['applications'] = Application::all();
        } else { # Default user, not Manager
            $context['applications'] = $request->user()->applications_as_client ?? null;
        }

        return response()
                ->view('index', $context);
    }
    public function show(Request $request, Application $application) {
        $sorted_collection = $application->messages->sortBy('created_at');
        $context = [
            'application' => $application,
            'head_message' => $sorted_collection->first(),
            'messages' => $sorted_collection,
        ]; # Creates a collection of all messages,
           # sorts by created_at- field, gets oldest
           # as head-message and excludes it from the
           # collection

        $context['messages']->shift();

        return response()
            ->view('show', $context);
    }
    public function store(StoreMessagePostRequest $request, Application $application = null) {
        $filename = null;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $filename = Str::uuid() . MessageFile::$uuid_name_separator . $file->getClientOriginalName();
            $file->move(public_path() . MessageFile::$path_to_user_files, $filename);
        }
        $is_app_created = false;
        if (!$application) {
            $is_app_created = true;
            $application = Application::create([
                'client_id' => $request->user()->id,
                'manager_id' => null,
                'is_closed' => false,
            ]);
        }

        $request['sender_id'] = $request->user()->id;
        $request['application_id'] = $application->id;

        $message_id = Message::create($request->except('_token'))->id;

        if ($filename) {
            MessageFile::create([
                'filename' => $filename,
                'message_id' => $message_id,
            ]);
        }

        if($is_app_created) {
            Mail::to(
                'manager@gmail.com'
            )->send(new ApplicationCreated($application));
        } # Application created, sending e-mail
        elseif($application->manager_id) {
            $eml = null;
            if(Auth::user()->id == $application->client_id) {
                $eml = $application->manager->email;
            }  else {
                $eml = $application->client->email;
            }

            Mail::to(
                $eml
            )->send(new ApplicationMessageCreated($application));
        }

        return redirect()
                ->action('ApplicationController@show', ['application' => $application->id]);
    }
    public static function start(Request $request, Application $application) {
        if(!$application->manager_id) {
            $application->manager_id = $request->user()->id;
            $application->save();
        }
        return redirect()
                ->action('ApplicationController@show', ['application' => $application->id]);
    }
    public static function finish(Request $request, Application $application) {
        if($request->user()->id == $application->client_id) {
            $application->is_closed = true;
            $application->save();

            if($application->manager) {
                Mail::to(
                    $application->manager->email
                )->send(new ApplicationClosed($application));
            }
        }

        return redirect()
            ->action('ApplicationController@show', ['application' => $application->id]);
    }
}
