<?php

namespace App\Http\Middleware;

use App\Application;
use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class ApplicationVerifyDays
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(count(\Illuminate\Support\Facades\Auth::user()->applications_as_client) == 0) {
            return $next($request);
        }
        if(@Carbon::now()->diffInDays(Auth::user()->applications_as_client->last()->created_at) <= 1) {
            if($request->has('__creating')) {
                throw new \Exception("You can make only one application per day", 403);
            } else {
                return $next($request);
            }
        } else {
            return $next($request);
        }
    }
}
