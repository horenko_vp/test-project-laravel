<?php

namespace App\Http\Middleware;

use App\Application;
use Closure;

class CheckAccessToApplication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->post('__creating', null)) {
            return $next($request);
        }

        $id = $request->user()->id;
        $application = Application::all()->find($request->route('application'));
        if($id == $application->client_id or $request->user()->hasRole('Manager')) {
            return $next($request);
        } else {
            throw new \Exception("Access denied", 403);
        }
    }
}
