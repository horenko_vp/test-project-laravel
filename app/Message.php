<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['title', 'description', 'application_id', 'sender_id'];
    public function application() {
        $this->belongsTo(Application::class, 'application_id', 'id');
    }
    public function file() {
        return $this->hasOne(MessageFile::class, 'message_id', 'id');
    }
    public function sender() {
        return $this->belongsTo(User::class, 'sender_id', 'id');
    }
}
