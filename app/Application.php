<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = ['client_id', 'manager_id', 'is_closed'];
    public function messages() {
        return $this->hasMany(Message::class, 'application_id', 'id');
    }

    public function client() {
        return $this->belongsTo(User::class, 'client_id', 'id');
    }
    public function manager() {
        return $this->belongsTo(User::class, 'manager_id', 'id');
    }
}
