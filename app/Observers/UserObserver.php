<?php

namespace App\Observers;

use App\User;
use Spatie\Permission\Models\Role;

class UserObserver
{
    public function created(User $user){

        $role = Role::findByName('User');
        $user->assignRole($role);

    }
}
