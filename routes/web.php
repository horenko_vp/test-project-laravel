<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action(
        'ApplicationController@index'
    );
});

Route::middleware(['auth'])->prefix('/applications')->group(function(){
    Route::get('/', 'ApplicationController@index'); # any sort of filters are only for Managers'- role
    Route::get('/create', 'ApplicationController@create')->middleware(['day_diff']);
    Route::middleware(['application_access'])->group(function() {
        Route::get('/{application}', 'ApplicationController@show');
        Route::post('/{application?}', 'ApplicationController@store')->middleware(['day_diff']);
        Route::get('/{application}/start_conversation', 'ApplicationController@start')
            ->middleware(['role:Manager']);
        Route::get('/{application}/discard', 'ApplicationController@finish');
    });
    Route::get('/download/{file}', 'ApplicationController@download');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
