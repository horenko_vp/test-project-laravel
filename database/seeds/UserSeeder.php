<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $seed_amount = 0;

    public function run()
    {
        if(\App\User::all()->where('email', 'manager@gmail.com')->count() == 0) {
            $user = \App\User::create([
                'name' => 'manager',
                'email' => 'manager@gmail.com',
                'email_verified_at' => now(),
                'password' => bcrypt('1as2d3asd4asd5sad6sads7a8d9dsdf'),
                'remember_token' => \Illuminate\Support\Str::random(10),
            ]);
            $manager_role = \Spatie\Permission\Models\Role::firstOrCreate(['name' => 'Manager']);
            $user->assignRole($manager_role);
        }

        factory(App\User::class, $this->seed_amount)->create();
    }
}
