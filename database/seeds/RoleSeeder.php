<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Spatie\Permission\Models\Role::firstOrCreate(['name' => 'User']);
        \Spatie\Permission\Models\Role::firstOrCreate(['name' => 'Manager']);
    }
}
