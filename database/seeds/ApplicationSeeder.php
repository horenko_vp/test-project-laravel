<?php

use Illuminate\Database\Seeder;

class ApplicationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $seed_app_amount = 0;
    private $seed_msg_amount = 0;
    public function run()
    {
        factory(\App\Application::class, $this->seed_app_amount)->create();
        factory(\App\Message::class, $this->seed_msg_amount)->create(); # for messages
    }
}
