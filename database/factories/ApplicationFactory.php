<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Application::class, function (Faker $faker) {
    $user_collection = \App\User::role('User')->get();
    $client = $user_collection->random();

    while($client->hasRole('Manager')) {
        $client = $user_collection->random();
    }

    $manager = \Illuminate\Support\Arr::random([null, \App\User::role('Manager')->get()]);

    if($manager) {
        $manager = $manager->random()->id;
    }

    return [
        'client_id' => $client->id,
        'manager_id' => $manager,
    ];
});
