<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Message::class, function (Faker $faker) {
    $application = \App\Application::all()->random();
    $sender_id = $faker->randomElement([$application->manager_id, $application->client_id]);

    return [
        'title' => $faker->words(random_int(3, 15)),
        'description' => $faker->realText(),
        'application_id' => $application->id,
        'sender_id' => $sender_id??$application->client_id,
    ];
});
